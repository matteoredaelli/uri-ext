(ns net.clojars.matteoredaelli.uri-ext
  (:require [lambdaisland.uri :refer [uri join]]))

;;; URI
(defn same-field?
  [uri1 uri2 field]
  (= (field uri1) (field uri2)))

(defn same-website?
  [uri1 uri2]
  (and (same-field? uri1 uri2 :host)
       (same-field? uri1 uri2 :port)
       (same-field? uri1 uri2 :scheme)))

(defn sub-uri?
  [uri1 uri2]
  (and (same-website? uri1 uri2)
       (clojure.string/starts-with? (:path uri2) (:path uri1))
       (same-field? uri1 uri2 :port)
       (same-field? uri1 uri2 :scheme)))


(defn get-domain
  [uri1]
  (str (assoc uri1 :path nil :fragment nil :query nil)))


;; URLS VECTORS

(defn remove-links-with-fragment
  [links]
  (filterv #(not (clojure.string/includes? % "#")) links)
  )

(defn remove-empty-links
  [links]
  (filterv #(not (= % ""))
           links)
  )

(defn filter-email-links
  [links]
  (filterv #(clojure.string/starts-with? % "mailto:")
           links))

(defn remove-links-with-mailto
  [links]
  (filterv #(not (clojure.string/starts-with? %
                                              "mailto:"))
           links)
  )

(defn filter-external-links
  [links address]
  (filterv #(not (same-website? (uri %)
                                (uri address)))
           links)
  )

(defn filter-internal-links
  [links address]
  (filterv #(same-website? (uri %)
                           (uri address))
           links)
  )
